# leggimi.ml

[![Flask 1.0.x support](https://img.shields.io/badge/flask-1.0.2-blue.svg)](http://flask.pocoo.org/)
[![License: AGPL v3+](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

[*Leggimi.ml*](https://leggimi.ml) è un sito web che permette a bambini ipovedenti o con difficoltà nel leggere di ascoltare ciò che hanno scritto.

## Licenza
Il sito è rilasciato sotto licenza [GNU Affero General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html) o versioni successive.

Se pensi che questo sito sia utile, considera la possibilità di offrirmi una [birra](https://patreon.com/tallero), un nuovo [computer](https://patreon.com/tallero) o un [lavoro](mailto:pellegrinoprevete@gmail.com) part-time da remoto che mi permetta di pagare le bollette e proseguire gli studi.
