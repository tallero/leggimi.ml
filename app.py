#!/usr/bin/env python3

# leggimi.ml
#
# Copyright (C) 2018 Pellegrino Prevete
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from flask import Flask, request, send_file
from os import makedirs
from os.path import exists, join
from subprocess import check_output as sh
app = Flask(__name__)

app_name = "leggimi.ml"

@app.route("/")
def hello():
    with open(join('templates', 'speaker.html')) as f:
        speaker = f.read()
        f.close()
    return speaker

@app.route("/parla", methods=['POST'])
def parla():
    keys = request.form.keys()
    if 'phrase' in keys and 'id' in keys:
        phrase = request.form['phrase']
        user = request.form['id']
        if type(phrase) == str and type(user) == str and not "/" in user:
            input_phrase = phrase[:100]
            work_dir = f"/tmp/{app_name}"
            if not exists(work_dir):
                mode = 0o755
                mkdirs("work_dir", mode)
            output_wav = join(work_dir, f"{user}.wav")
            output_mp3 = join(work_dir, f"{user}.mp3")
            command = {"espeak": ['espeak', '-s', '200',
                                            '-a', '300',
                                            '-v', 'it',
                                            f"{input_phrase}.",
                                            '-w', output_wav],
                       "mimic": ['mimic', '-t', input_phrase,
                                          '-o', output_wav],
                       "ffmpeg": ['ffmpeg', '-y',
                                            '-i', output_wav,
                                            '-ar', '44100',
                                            '-ac', '2',
                                            '-ab', '192k',
                                            '-f', 'mp3',
                                            output_mp3]}
            sh(command['espeak'])
            sh(command['ffmpeg'])
            res = send_file(output_mp3,
                            download_name=f"{user}.mp3")
            return res
    else:
        return request.method
