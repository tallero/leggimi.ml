/* leggimi.ml: speaker.js

 Copyright (C) 2018 Pellegrino Prevete
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

class Speaker {
    constructor(user) {
        this.colours = ['#62a0eaff',
                        '#57e389ff',
                        '#f6d32dff',
                        '#ffa348ff',
                        '#ed333bff',
                        '#c061cbff',
                        '#b5835aff',
                        '#9a9996ff']
        this.user = user;
        this.time = 0;
        this.speed = 3000;
        //this.slider = document.getElementById('speed');
        //console.log(this.slider);
        //let speed;
        //this.speed = this.slider.value;
        //this.slider.oninput = () => {
        //    speed = this.value;
        //}
        //this.speed = speed;
        this.events = "";
        document.addEventListener("keypress", this.grab.bind(this), false); 
    }

    grab(e) {
        clearTimeout(this.trigger)
        let unicode = String.fromCharCode(e.which);
        this.events += unicode;
        let speed = (Date.now() - this.time);
        console.log('actual speed: ' + speed);
        console.log('old speed: '+this.speed);
        this.time = Date.now();
        document.getElementById("title").innerHTML = this.events;
        let thres = Math.min(100*Math.log(speed), this.speed);
        console.log("threshold: "+thres);
        this.trigger = setTimeout(this.play.bind(this), thres);
    }
   
    play() {
        let form = new FormData();
        form.append("phrase", this.events);
        form.append('id', this.user.id);
        let request = new Request('./parla', {'method':'POST', 'body':form});
        fetch(request)
        .then(this.returnBlob)
        .then(this.processBlob.bind(this))
        .catch(function(error) {
          document.body.insertAdjacentHTML( 'afterbegin', 'Error: ' + error.message);
        });
    }

    returnBlob(response) {
        if (!response.ok) {
            throw new Error("HTTP error, status = " + response.status);
        }
        return response.blob();
    }

    processBlob(blob) {
        this.events = ""
        let url = URL.createObjectURL(blob);
        let audio = new Audio();
        audio.src = url;
        console.log(audio);
        audio.play();
        audio.onended = () => {
                                  //this.events = "";
                                  let colour = this.colours[parseInt(Math.random()*(this.colours.length-1))]
                                  $("body").css("background-color", colour);
                              };
    }
}

class User {
    constructor() {
        if (document.cookie == '') {
            let n = this.create_id(100);
            this.id = n;
            document.cookie = document.cookie + "id=" + n + ";expires=Thu, 18 Dec 2030 12:00:00 UTC; path=/";
            }
        else {
            this.id = document.cookie.split("id=")[1];
        }
    }

    create_id(len) {
       let admissable = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       let id = "";
       for (let i=0; i < len; i++) { 
           id += admissable[parseInt(admissable.length*Math.random())];
       }
       return id
    }
}
$(document).ready(function() {
    user = new User();
    speaker = new Speaker(user);
});
